require 'net/http'
require 'open-uri'


def main()
  
  arguments_count = ARGV.length
  if arguments_count == 1 && ARGV[0] == "--help"
    displayHelp()
  elsif arguments_count < 2
    puts %Q{
Error: Function Misused - Consult Help Page (ruby get.rb --help)

Explanation: The 'get' command requires at least 2 parameters to work correctly. Please review the Help section by
typing 'ruby get.rb --help' to learn the correct usage of the 'get' command.

    }
  else
    document = ARGV[0]
    web_address = ARGV[1]
   
    if web_address.to_s.end_with? "/"
      web_address = web_address.slice(0..-2)
    end
    if !web_address.include? "http://" || (!web_address.include? "https://")
      web_address = "http://" + web_address
    end
    web_address = web_address + "/" + document
    
    result = Net::HTTP.get_response(URI.parse(web_address))
    if result.code == "301" || result.code == "302"
      result = Net::HTTP.get_response(URI.parse(result.header['location']))
    end
    puts result.body
  end
 
 

end

def displayHelp()
  puts %Q{
Hi and Welcome to the Get Command Help Page.

The 'get' command requires 2 parameters:
  * Parameter 1 -> HTML Page you wish to load from a given web server, for example 'index.html'
  * Parameter 2 -> Web Address of the server to which you wish to make a request, for example 'http://www.google.com'
      -- Special Note -> You do not need to worry about adding 'http' or 'www' to the web address if you do not want to do so

***Example Usage***
  Normal Usage: ruby get.rb index.html byu.edu


If you have further questions regarding the usage of ruby, please visit http://ruby-doc.org/
  }
end

if __FILE__ == $PROGRAM_NAME
 main()
end